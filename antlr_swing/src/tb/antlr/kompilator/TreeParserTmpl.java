/**
 * 
 */
package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;

/**
 * @author tb
 *
 */
public class TreeParserTmpl extends TreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	protected LocalSymbols mLocalSymbols = new LocalSymbols();
	protected int mDepth = 0;
	
	/**
	 * @param input
	 */
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param input
	 * @param state
	 */
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		// TODO Auto-generated constructor stub
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
	
	protected String getLocalName(String name) {
		return name + "__" + mDepth;
	}
	
	protected void declareVariable(String name) {
		if (!mLocalSymbols.hasSymbol(name)) {
			mLocalSymbols.newSymbol(name);
		}
	}
	
	protected void checkVariable(String name) {
		if(!mLocalSymbols.hasSymbol(name)) {
			throw new RuntimeException("Symbol \"" + name + "\" not found.");
		}
	}

	protected void enterScope() {
		mLocalSymbols.enterScope();
		++mDepth;
	}
	
	protected void leaveScope() {
		mLocalSymbols.leaveScope();
		--mDepth;
	}
}