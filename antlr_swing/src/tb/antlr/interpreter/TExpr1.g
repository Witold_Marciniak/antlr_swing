tree grammar TExpr1;

options {
  tokenVocab   = Expr;
  ASTLabelType = CommonTree;
  superClass   = MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog
  :
  (
    expr
    | print
    | block
  )*
  ;

print
  :
  ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString()); }
  ;

block
  : LB  {enter(); }
  | RB  {leave(); }
  ;

expr returns [Integer out]
  : ^(PLUS e1=expr e2=expr){$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(MOD   e1=expr e2=expr) {$out = modulo($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = power($e1.out, $e2.out);}
        | ^(VAR id=ID) {declare($id.text);}
        | ^(PODST id=ID e1=expr) {set($id.text, $e1.out);
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = get($ID.text);}

        ;
